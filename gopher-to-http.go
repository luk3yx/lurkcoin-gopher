//
// Listens for Gopher requests and sends them to a remote server as
// Flask-Gopher compatible HTTP and fixes the server address in the response.
//
// The Flask-Gopher code should use `request.args.get('search_text', '')` to
// obtain the search text if it detects HTTP(S) as I can't see a way to make
// Flask-Gopher set the SEARCH_TEXT environment variable from HTTP.
//
// Copyright © 2021 by luk3yx.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package main

import (
	"bufio"
	"bytes"
	// "fmt"
	"io"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"time"
)

type server struct {
	gopherHost string
	gopherPort string
	listener   net.Listener
	httpUrl    *url.URL
	httpClient *http.Client
}

func (s server) run() {
	for {
		conn, err := s.listener.Accept()
		if err != nil {
			log.Fatal(err)
		}
		go s.handleConnection(conn)
	}
}

const timeout = 30 * time.Second

func (s server) handleConnection(conn net.Conn) {
	defer conn.Close()

	if err := conn.SetReadDeadline(time.Now().Add(timeout)); err != nil {
		return
	}

	line, err := bufio.NewReader(conn).ReadBytes('\n')
	if err != nil {
		return
	}

	// Remove trailing \r\n (or \n) and leading /
	line = bytes.TrimRight(line, "\r\n")
	line = bytes.TrimPrefix(line, []byte{'/'})

	// Split the URL and search text and add them to the base URL.
	// The "u" variable is a copy of s.httpUrl and is not a pointer.
	u := *s.httpUrl
	var search_text string
	if tab_idx := bytes.IndexByte(line, '\t'); tab_idx >= 0 {
		search_text = string(line[tab_idx+1:])
		line = line[:tab_idx]
	}

	// Split the query string (for _session)
	if query_idx := bytes.IndexByte(line, '?'); query_idx >= 0 {
		u.Path += string(line[:query_idx])
		u.RawQuery = string(line[query_idx+1:])
	} else {
		u.Path += string(line)
	}

	// Add the search text onto the query
	if len(search_text) > 0 {
		if len(u.RawQuery) > 0 {
			u.RawQuery = "&" + u.RawQuery
		}
		u.RawQuery = "search_text=" + url.QueryEscape(search_text) + u.RawQuery
	}

	// Make the HTTP request
	// TODO: Pass the url.URL instance directly into net/http
	rawUrl := u.String()
	// fmt.Printf("Got URL %#v\n", rawUrl)
	resp, err := s.httpClient.Get(rawUrl)
	if err != nil {
		log.Fatal(err)
	}

	defer resp.Body.Close()
	// fmt.Println(resp)

	// The Content-Type used for Gopher pages is text/html for some reason.
	if !strings.HasPrefix(resp.Header.Get("Content-Type"), "text/html") {
		io.Copy(conn, resp.Body)
		return
	}

	// Ensure the file doesn't have an extension (as it may be an HTML file).
	slash_idx := strings.LastIndexByte(u.Path, '/')
	if slash_idx < 0 {
		slash_idx = 0
	}
	if strings.IndexByte(u.Path[slash_idx:], '.') >= 0 {
		if resp.StatusCode == 200 {
			io.Copy(conn, resp.Body)
		} else {
			io.WriteString(conn, "Error: ")
			io.WriteString(conn, strings.Title(strings.ToLower(resp.Status)))
			io.WriteString(conn, "\r\n")
		}

		return
	}

	// Return non-200 status codes
	if resp.StatusCode != 200 {
		io.WriteString(conn, "iError: ")
		io.WriteString(conn, strings.Title(strings.ToLower(resp.Status)))
		io.WriteString(conn, "\tfake\texample.com\t0\r\n.\r\n")
		return
	}

	// Otherwise add the correct domain in
	reader := bufio.NewReader(resp.Body)
	for {
		line, err := reader.ReadString('\n')
		if err != nil {
			// Write the remaining data and exit
			io.WriteString(conn, line)
			break
		}

		l := strings.Split(line, "\t")
		if len(l) != 4 {
			io.WriteString(conn, line)
			io.Copy(conn, reader)
			break
		}

		// Replace the domain if required
		if l[2] == "::" || l[2] == "0.0.0.0" {
			l[2] = s.gopherHost
			l[3] = s.gopherPort + "\r\n"
			io.WriteString(conn, strings.Join(l, "\t"))
		} else {
			io.WriteString(conn, line)
		}
	}
}

func startServer(network, address, publicAddress, httpUrl string) {
	var changeSocketPermissions bool
	if network == "unix" {
		if !strings.HasSuffix(address, ".socket") {
			log.Fatalf("Address %#v must end in .socket", address)
		}

		os.Remove(address)
		if stat, err := os.Stat(filepath.Dir(address)); err == nil {
			changeSocketPermissions = stat.Mode()&022 == 0
		}
	}

	listener, err := net.Listen(network, address)
	if err != nil {
		log.Fatal(err)
	}
	defer listener.Close()

	if changeSocketPermissions {
		if err := os.Chmod(address, 0777); err != nil {
			log.Fatal(err)
		}
	}

	parsedUrl, err := url.Parse(httpUrl)
	if err != nil {
		log.Fatal(err)
	}

	n := strings.SplitN(publicAddress, ":", 2)
	gopherHost := n[0]
	gopherPort := "70"
	if len(n) > 1 {
		gopherPort = n[1]
	}

	client := &http.Client{Timeout: 2 * time.Minute}
	server := server{gopherHost, gopherPort, listener, parsedUrl, client}
	log.Println("Starting server on", address)
	log.Printf("Public address: gopher://%s:%s/", gopherHost, gopherPort)
	log.Println("Target HTTP URL:", httpUrl)
	server.run()
}

func conf(name, defaultValue string) string {
	res := os.Getenv("GOPHER_PROXY_" + name)
	if res == "" {
		return defaultValue
	}
	return res
}

func main() {
	startServer(
		conf("BIND_PROTO", "tcp"),
		conf("BIND_ADDR", "127.0.0.1:7070"),
		conf("PUBLIC_ADDR", "localhost:7070"),
		conf("HTTP_URL", "http://lurkcoin.minetest.land:70"),
	)
}
