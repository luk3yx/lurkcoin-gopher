#!/usr/bin/python3
#
# lurkcoin Gopher
#
# Copyright © 2018-2021 by luk3yx.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

from flask import Flask, session, request
from flask_gopher import GopherRequestHandler, GopherExtension
import json, os, requests, socket

# Create the app
app = Flask(__name__)
app.config.update(SERVER_NAME=os.getenv('SERVER_NAME'), APPLICATION_ROOT='/')
app.secret_key = os.environ.get('SECRET_KEY',
                                socket.gethostname()).encode('utf-8')
gopher = GopherExtension(app)

# More variables
baseurl = (os.environ.get('LURKCOIN_BASEURL', 'https://lurkcoin.minetest.land')
           + '/v2')
proxy_api = frozenset({'exchange_rates'})

# Get data from lurkcoin
def _get(url, parse_json=True, **data):
    try:
        res = requests.post(f'{baseurl}/{url}', data=data, timeout=10)
    except:
        res = None

    if res is None:
        return 500, 'ERROR: Could not connect to lurkcoin!'

    text = str(res.text)

    # Parse JSON automatically
    if (parse_json and isinstance(res.headers.get('Content-Type'), str) and
            res.headers['Content-Type'].startswith('application/json')):
        try:
            text = json.loads(text)
        except:
            pass

    return res.status_code, text

# Get the interest rate
_c, interest_rate = _get('interest_rate')
if _c == 404:
    interest_rate = 0
else:
    assert _c == 200 and isinstance(interest_rate, (int, float))
del _c

# Render templates
def r(_name, **v):
    mobile = session.get('mobile') and 'm' or ''
    v['page_name'] = _name
    return gopher.render_menu_template(f'{_name}.gopher{mobile}', **v)

# Get the search text (along with a compatibility layer for HTTP)
def get_search_text():
    if request.scheme == 'gopher':
        # Flask-Gopher adds SEARCH_TEXT to request.environ
        return request.environ.get('SEARCH_TEXT', '')
    else:
        # Allow HTTP requests to specify the search text in a query parameter
        return request.args.get('search_text', '')

# index.gopher
@app.route('/')
def main_page():
    return r('index', interest_rate=interest_rate)

# Exchange rate calculation
@app.route('/exchange-rates')
@app.route('/exchange-rates/<path:servers>')
def exchange_rates(servers = ''):
    servers = (servers or '') + ' ' + get_search_text()
    servers = servers.replace('\t', ' ').strip().split(' ')
    if servers and servers[0]:
        # Deduplicate and sort list
        servers = set(servers)
        if '' in servers:
            servers.remove('')
        servers = list(servers)
        servers.sort()
    else:
        return r('exchange-410')

    rates = [['Server name', 'From lurkcoin', 'To lurkcoin']]

    for s in servers:
        if s.lower() == 'lurkcoin':
            rates.append(['lurkcoin', 1, 1])
        else:
            code, e = _get('exchange_rates', to=s)
            if code != 200 or not isinstance(e, (int, float)):
                o = list(servers)
                o.remove(s)
                return r('exchange-404', name=repr(s or '???'),
                    others=' '.join(o))

            rates.append([s, e, 1 / e])

    return r('exchange-200', result=rates, servers=' '.join(servers),
        plural='' if len(servers) == 1 else 's')

@app.route('/exchange-rates/')
def exchange_410():
    return r('exchange-410')

# About page
@app.route('/about')
def about():
    return r('about')

# Switch to desktop mode
@app.route('/desktop')
def force_desktop():
    # if 'mobile' in session:
    #     del session['mobile']
    session.clear()
    return main_page()

# Switch to mobile mode
@app.route('/m')
@app.route('/mobile')
def force_mobile():
    session['mobile'] = True
    return main_page()

# Fix weird link breaking
@app.route('/https')
def https_site():
    return """<!DOCTYPE html>
    <title>Redirecting...</title>
    <script>window.location.href='https://lurkcoin.minetest.land';</script>
    <meta http-equiv="refresh" content="0; url=https://lurkcoin.minetest.land">
    <a href='https://lurkcoin.minetest.land'>Click here if you are not
     redirected.</a>
    """.replace('\n    ', '')

# Proxy allowed lurkcoin API functions
@app.route('/v2/<path:endpoint>')
def api_proxy(endpoint=''):
    if endpoint == 'interest_rate':
        return str(interest_rate)
    elif endpoint in proxy_api:
        search = get_search_text()

        if search:
            code, msg = _get(endpoint + '?' + search, False)
        else:
            code, msg = _get(endpoint, False, **request.args)

        if code != 200 and not msg.startswith('ERROR: '):
            msg = 'ERROR: ' + msg

        return msg
    else:
        return 'ERROR: You cannot access that endpoint via Gopher!'


# Start the app
if __name__ == '__main__':
    app.run(host='::', threaded=True, port=70,
        request_handler=GopherRequestHandler)
